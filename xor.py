#!/usr/bin/python3

from PIL import Image
from argparse import ArgumentParser

def main():
    parser = ArgumentParser(description="Xors colors of two images into a new one. It also does scaling. Original images are unchanged.")
    parser.add_argument("path1", metavar="PATH-1", type=str, help="Path to first xored image.")
    parser.add_argument("path2", metavar="PATH-2", type=str, help="Path to second xored image.")
    parser.add_argument("width", metavar="WIDTH", type=int, help="Width of the final image.")
    parser.add_argument("height", metavar="HEIGHT", type=int, help="Height of the final image.")
    parser.add_argument("finalpath", metavar="OUT-PATH", type=str, help="Path of the output image.")
    args=parser.parse_args()
    xor(args.path1, args.path2, (args.width, args.height), args.finalpath)

def xor(pic1path, pic2path, size, finalpicpath):
    pic1 = Image.load(pic1path).resize(size).load()
    pic2 = Image.load(pic2path).resize(size).load()
    final = Image.new("RGB", size)
    final_px = final.load()
    for x in range(size[0]):
        for y in range(size[1]):
            col1 = pic1[x, y]
            col2 = pic2[x, y]
            final_px[x, y] = (col1[0]^col2[0], col1[1]^col2[1], col1[2]^col2[2])

    final.save(finalpicpath)

if __name__ == "__main__":
    main()
