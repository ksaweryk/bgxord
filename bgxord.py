#!/usr/bin/python3

##    Copyright (C) 2018 Ksawery Korzeniewski

##    This program is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    This program is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from daemon import DaemonContext
from daemon.pidfile import PIDLockFile
import os
import os.path
from PIL import Image
import requests

import asyncio
from aiohttp import web
from configparser import ConfigParser
from argparse import ArgumentParser

class Bgxord:
    def __init__(self):

        confpar = ConfigParser()
        confpar.read("bgxord.conf")
        conf = confpar["DEFAULT"]

        self.HOST = conf.get("host") or "127.0.0.1"
        self.PORT = int(conf.get("port")) or 8090
        self.LOG_FILE = conf.get("log_file") or "stdouterr.log"
        self.PID_FILE = conf.get("pid_file") or "bgxord.pid"
        self.PIC1 = conf.get("pic1") or "pic1.jpg"
        self.PIC2 = conf.get("pic2") or "pic2.jpg"
        self.FINAL_PIC = conf.get("final_pic") or "pic.jpg"
        self.HEADERS = {"User-Agent": conf.get("user-agent") or "linuxmint:bgxord:v1 (by /u/ksaweryk)"}
        self.SUBREDDIT = conf.get("subreddit")
        self.PIC_SIZE = (int(conf.get("pic_width")) or 1920, int(conf.get("pic_height")) or 1080)
        self.SET_BG = conf.get("bg_command") or ""
        self.LOG_LENGTH = conf.get("log_length") or 50
        self.FAVICON = "bgxord.png"
        self.PWD = os.path.dirname(os.path.abspath(__file__))
        self.DAEMON = True


    async def index_handler(self, request):
        """
        Serves index file with view of the log file
        """
        with open(os.path.join(self.PWD, "index.html"), "r") as indexf, open(os.path.join(self.PWD, self.LOG_FILE), "r") as logf:
            return web.Response(status=200, text=indexf.read().format("<br/>".join(logf.readlines()[:-self.LOG_LENGTH-1:-1])), headers={"Content-type": "text/html"})

    async def favicon_handler(self, request):
        """
        Serves favicon file
        """
        with open(os.path.join(self.PWD, self.FAVICON), "rb") as iconf:
            return web.Response(status=200, body=iconf.read(), headers={"Content-type": "image"})

    async def img_handler(self, request):
        """
        Serves various image files
        """
        which = request.match_info.get("n", "f")
        if which == "f":
            with open(os.path.join(self.PWD, self.FINAL_PIC), "rb") as picf:
                return web.Response(status=200, body=picf.read(), headers={"Content-type": "image"})
        elif which == "1":
            with open(os.path.join(self.PWD, self.PIC1), "rb") as picf:
                return web.Response(status=200, body=picf.read(), headers={"Content-type": "image"})
        elif which == "2":
            with open(os.path.join(self.PWD, self.PIC2), "rb") as picf:
                return web.Response(status=200, body=picf.read(), headers={"Content-type": "image"})
        else:
            return web.Response(status=404, text="404: Not Found")

    async def command_handler(self, request):
        """
        Handles posted commands
        """
        data = await request.post()
        if data.get("cmd") == "change":
            #asyncio.ensure_future(change())
            await self.change()
            print("Desktop changed")
            return web.Response(status=302, headers={"Location": "/"})

        elif data.get("cmd") == "kill":
            if self.DAEMON:
                with open(os.path.join(self.PWD, self.PID_FILE), "r") as pidf:
                    pid = pidf.read()
                    print("killing "+pid)
                    request.app.loop.call_later(1, exit)
                    # calls exit with 1s delay, enough to respond to the request
                    # not the best way of handling kill, but for now it's good enough
                    return web.Response(status=200, text="Killed "+pid)
            else:
                return web.Response(status=200, text="Not a daemon!")

        elif data.get("cmd") == "clear":
            with open(os.path.join(self.PWD, self.LOG_FILE), "w"): pass
            return web.Response(status=302, headers={"Location": "/"})

    async def change(self):
        """
        Changes the desktop background
        """
        await self.rand_pic(os.path.join(self.PWD, self.PIC1))
        await asyncio.sleep(1)
        await self.rand_pic(os.path.join(self.PWD, self.PIC2))
        pic1 = Image.open(os.path.join(self.PWD, self.PIC1)).resize(self.PIC_SIZE).load()
        pic2 = Image.open(os.path.join(self.PWD, self.PIC2)).resize(self.PIC_SIZE).load()
        final = Image.new("RGB", self.PIC_SIZE)
        final_px = final.load()
        for x in range(self.PIC_SIZE[0]):
            for y in range(self.PIC_SIZE[1]):
                col1 = pic1[x, y]
                col2 = pic2[x, y]
                final_px[x, y] = (col1[0]^col2[0], col1[1]^col2[1], col1[2]^col2[2])

        final.save(os.path.join(self.PWD, self.FINAL_PIC))
        os.system(self.SET_BG.format(os.path.join(self.PWD, self.FINAL_PIC)))


    async def rand_pic(self, where):
        """
        Saves a random image from a predetermined subreddit
        """
        with open(where, "bw") as picf:
            while 1:
                try:
                    # TODO: change http client from requests to aiohttp
                    pic_data = requests.get("https://api.reddit.com/r/" + self.SUBREDDIT + "/random", headers=self.HEADERS).json()[0]["data"]["children"][0]["data"]
                    pic_url = pic_data["url"]
                    break
                except KeyError:
                    await asyncio.sleep(2)
            #print(pic_url)
            for chunk in requests.get(pic_url, headers=self.HEADERS, stream=True).iter_content(1024):
                picf.write(chunk)
        return dict(author=pic_data.get("author"), source=pic_data.get("permalink"))

    def main(self):
        """
        Runs the web app
        """
        app = web.Application()
        app.router.add_route('GET', '/', self.index_handler)
        app.router.add_route('GET', '/favicon.ico', self.favicon_handler)
        app.router.add_route('GET', '/img/{n}', self.img_handler)
        app.router.add_route('POST', '/', self.command_handler)

        web.run_app(app, host=self.HOST, port=self.PORT, print=lambda *x: None)

    def run(self):
        parser = ArgumentParser(description="A background changing daemon.")
        parser.add_argument("-d", "--debug", action="store_true", help="Run in no daemon (debug) mode.")
        args = parser.parse_args()

        if args.debug:
            print("No daemon mode!")
            self.DAEMON = False
            self.main()

        else:
            try:
                with open(os.path.join(self.PWD, "bgxord.pid")) as pidf:
                    try:
                        os.kill(int(pidf.read()), 9)
                    except ProcessLookupError:
                        pass
                if os.path.isfile(os.path.join(self.PWD, "bgxord.pid")):
                    os.remove(os.path.join(self.PWD, "bgxord.pid"))
            except FileNotFoundError:
                pass

            with open(os.path.join(self.PWD, self.LOG_FILE), "a") as outf:
                with DaemonContext(pidfile=PIDLockFile(os.path.join(self.PWD, self.PID_FILE)), stdout=outf, stderr=outf):
                    self.main()

if __name__ == "__main__":
    main = Bgxord()
    main.run()
