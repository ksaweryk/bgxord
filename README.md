# bgxord
A background changing daemon.

A program written in python3.5 for downloading random images from a chosen subreddit, resizing them, xoring their colors and setting the processed image as your computer's desktop background.

Usage: `python3 bgxord.py`

You can also use `python3 xor.py {first image} {second image} {width} {height} {output path}` to xor the colors of two images without all the other functionality and `python3 change.py` to request daemon to change your desktop background.

Requires `python-daemon`, `docutils`, `Pillow`, `aiohttp` and `requests`.

You can install all the requirements using `pip install -r requirements.txt` in the repo's root directory.

Make sure to set correct background changing command in the config. The daemon binds to port 8090 of localhost by default. If it collides with other services, you can change it in the config file as well.
